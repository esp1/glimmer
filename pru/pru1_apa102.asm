;*
;* Copyright (C) 2016 Zubeen Tolani <ZeekHuge - zeekhuge@gmail.com>
;*
;* This file is as an example to show how to develop
;* and compile inline assembly code for PRUs
;*
;* This program is free software; you can redistribute it and/or modify
;* it under the terms of the GNU General Public License version 2 as
;* published by the Free Software Foundation.

  .cdecls "main_pru1.c", "am335x_pru.h", "gpo.h", "pov_geom.h"
  .cdecls
%{
#define END_FRAME_LENGTH ((LED_STRIP_LENGTH / 2) + 1)
#define LAST_PIXEL_IDX (LED_STRIP_LENGTH - 1)

// Program registers
#define r_delay R26
#define r_gpo_out R27

// Program arguments (loaded into contiguous registers starting at R14)
#define r_red_hi R14
#define r_red_lo R15
#define r_green_hi R16
#define r_green_lo R17
#define r_blue_hi R18
#define r_blue_lo R19
#define r_idx R20.b0
%}

NOP .macro
  MOV r_delay, r_delay
  .endm

long_delay .macro
  LDI32 r_delay, 10000
  QBEQ $E?, r_delay, 0
$M?:
  SUB r_delay, r_delay, 1
  QBNE $M?, r_delay, 0
$E?:
  .endm

delay .macro
  NOP
  NOP
  NOP
;  long_delay
  .endm

; Clock out value on R_GPO
clock_out_gpo: .macro
  delay
  ; data + clock high
  ; clock bit is in byte 1, so some special syntax is required
  OR R_GPO.b1, R_GPO.b1, CLK_ALL_GPO_B1_MASK
  delay
  ; data + clock low
  LDI32 R_GPO, 0
  delay
  .endm

; Clocks out val to the GPO register n times.
out_n .macro val, n
  LOOP $end_out_n?, n
  ; data high, clock low
  LDI32 R_GPO, val
  clock_out_gpo
$end_out_n?:
  .endm

; Sends a start frame consisting of a sequence of 32 low bits for all LED GPOs.
start_frame .macro
  out_n 0, 32
  .endm

; Sends the led frame start sequence,
; which is 3 high bits for the frame start,
; followed by 5 more high bits to indicate full brightness.
led_frame_start .macro
  out_n S_ALL_GPO_B0_MASK, 8
  .endm

; Sends an end frame consisting of (LED_STRIP_LENGTH / 2) clock pulses
; with all LED GPO bits set high.
end_frame .macro
  out_n S_ALL_GPO_B0_MASK, END_FRAME_LENGTH
  .endm

; Sends data to all LED GPOs simultaneously.
; Takes a 32-bit data value containing 4 bits of GPO data for each of the 8 LED strips
; Takes advantage of the fact that all 8 GPOs are contiguous bits 0-7 of the output GPO register,
; so data can be written out by simply shifting and masking bytes of the input data.
led_frame_slice .macro r_data_hi, r_data_lo
  LOOP $end_color_lo?, 3
  ; data high, clock low
  MOV R_GPO.b0, r_data_lo.b3
  clock_out_gpo
  ; shift left one byte
  LSL r_data_lo, r_data_lo, 8
$end_color_lo?
  ; data high, clock low
  MOV R_GPO.b0, r_data_lo.b3
  clock_out_gpo

  LOOP $end_color_hi?, 3
  ; data high, clock low
  MOV R_GPO.b0, r_data_hi.b3
  clock_out_gpo
  ; shift left one byte
  LSL r_data_hi, r_data_hi, 8
$end_color_hi?
  ; data high, clock low
  MOV R_GPO.b0, r_data_hi.b3
  clock_out_gpo
  .endm

  .clink
  .global pru1_apa102_show_pixel
pru1_apa102_show_pixel:
  QBNE $led_frame?, r_idx, 0
  start_frame

$led_frame?:
  led_frame_start ; (8 bits)
  led_frame_slice r_blue_hi, r_blue_lo
  led_frame_slice r_green_hi, r_green_lo
  led_frame_slice r_red_hi, r_red_lo

  QBNE $end?, r_idx, LAST_PIXEL_IDX
  end_frame

$end?:
  ; Continue calling program
  JMP R3.w2

