#ifndef __pru_pixel_h__
#define __pru_pixel_h__


// Use uint32_t for idx to avoid byte alignment gaps. See http://www.catb.org/esr/structure-packing/
typedef struct {
  uint64_t red;
  uint64_t green;
  uint64_t blue;
  uint8_t idx;
} PixelData;


#endif //__pru_pixel_h__
