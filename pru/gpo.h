// Note: all of the below are on PRU1, GPIO2
// R30 (GPO) bits:

#define A3S1_GPO_B0_MASK	(0x1 << 3)
#define A3S0_GPO_B0_MASK	(0x1 << 6)
#define A2S1_GPO_B0_MASK	(0x1 << 5)
#define A2S0_GPO_B0_MASK	(0x1 << 2)
#define A1S1_GPO_B0_MASK	(0x1 << 7)
#define A1S0_GPO_B0_MASK	(0x1 << 4)
#define A0S1_GPO_B0_MASK	(0x1 << 1)
#define A0S0_GPO_B0_MASK	(0x1 << 0)

#define S_ALL_GPO_B0_MASK	A0S0_GPO_B0_MASK | A0S1_GPO_B0_MASK \
				| A1S0_GPO_B0_MASK | A1S1_GPO_B0_MASK \
				| A2S0_GPO_B0_MASK | A2S1_GPO_B0_MASK \
				| A3S0_GPO_B0_MASK | A3S1_GPO_B0_MASK

#define CLK0_GPO_B1_MASK	(0x1 << (9 - 8))
#define CLK1_GPO_B1_MASK	(0x1 << (8 - 8))

#define CLK_ALL_GPO_B1_MASK	CLK0_GPO_B1_MASK | CLK1_GPO_B1_MASK
