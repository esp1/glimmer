/*
 * More meaningful symbolic references to AM335x PRU internals.
 * Information is taken from the AM335x and AMIC110 Technical Reference Manual.
 * Relevant sections are referenced below.
 */

#ifndef _AM335X_PRU_H_
#define _AM335X_PRU_H_

// 4.4.1.1 Constants Table
// Table 4.9 PRU0/1 Constants Table
#define CONST_PRU_INTC C0
#define CONST_PRU_DRAM C24

// 4.4.1.2.1 Real-Time Status Interface Mapping (R31): Interrupt Events Input
// Table 4.10 Real-Time Status Interface Mapping (R31) Field Descriptions
#define HOST1_INT 31
#define HOST0_INT 30

// 4.4.1.2 PRU Module Interface to PRU I/Os and INTC
// 4.4.1.2.1 Interrupt Events Input
#define R_PRU_INT R31
// 4.4.1.2.2 PRU System Events
#define R_PRU_SYSEV R31
#define R31_VECTOR_VALID_STROBE_BIT 5
// 4.4.1.2.3 General Purpose Inputs
#define R_GPI R31

#define R_GPO R30 // 4.4.1.2.4 General Purpose Outputs

// 4.5 Registers
#define SICR_PRU_INTC_OFFSET 0x24 // 4.5.3.6 SICR Register

#endif /* _AM335X_PRU_H_ */
