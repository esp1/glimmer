#! /bin/bash

SCRIPT_DIR=$(cd `dirname $0` && pwd)

function config-pru-pin {
  config-pin -a $1 pruout
  config-pin -q $1
}

echo "Configuring pinmux"
  config-pru-pin P8_40 # A2S1
  config-pru-pin P8_42 # A1S1
  config-pru-pin P8_44 # A0S1
  config-pru-pin P8_46 # A3S1
  config-pru-pin P8_45 # A3S0
  config-pru-pin P8_43 # A0S0
  config-pru-pin P8_41 # A2S0
  config-pru-pin P8_39 # A1S0
  config-pru-pin P8_29 # CLK0
  config-pru-pin P8_27 # CLK1

echo "Installing PRU code"
  cd $SCRIPT_DIR
  export PRU_CGT=/usr/share/ti/cgt-pru
  make install

echo "PRU code installed"
