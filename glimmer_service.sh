#! /bin/bash

SCRIPT_DIR=$(cd `dirname $0` && pwd)

echo "Removing Glimmer kernel module"
rmmod glimmer

# Build and install PRU code
$SCRIPT_DIR/pru/deploy.sh

echo "Installing virtio_rpmsg_bus"
modprobe virtio_rpmsg_bus > /dev/kmsg 2>&1
echo "Installing Glimmer kernel module" > /dev/kmsg
insmod $SCRIPT_DIR/kernel_module/glimmer.ko > /dev/kmsg 2>&1
echo "Glimmer kernel module installed" > /dev/kmsg
