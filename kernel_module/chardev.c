#include <linux/device.h>
#include <linux/fs.h>
#include <linux/mutex.h>
#include <asm/uaccess.h>
#include "pov_geom.h"
#include "display.h"

// /dev/DEVICE_NAME
// /sys/class/CLASS_NAME/DEVICE_NAME
#define  DEVICE_NAME "glimmer"
#define  CLASS_NAME  "glimmer"

static DEFINE_MUTEX(glimmer_mutex);

// State
static int    majorNumber;                   // Stores the device number -- determined automatically
static struct class*  glimmerClass  = NULL;  // The device driver class struct pointer
static struct device* glimmerDevice = NULL;  // The device driver device struct pointer

static char   message[256] = {0};  // Memory for the string that is passed from userspace
static short  size_of_message;     // Used to remember the size of the string stored

// Function prototypes
static int     dev_open(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);
static int     dev_release(struct inode *, struct file *);

static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};

int glimmer_chardev_init(void) {
  int result = 0;

  mutex_init(&glimmer_mutex);

  // Try to dynamically allocate a major number for the device -- more difficult but worth it
  majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
  if (majorNumber<0) {
    pr_alert("GLIMMER: Failed to register a major number\n");
    return majorNumber;
  }
 
  // Register the device class
  glimmerClass = class_create(THIS_MODULE, CLASS_NAME);
  if (IS_ERR(glimmerClass)) {
    unregister_chrdev(majorNumber, DEVICE_NAME);
    pr_alert("GLIMMER: Failed to register device class\n");
    return PTR_ERR(glimmerClass);  // Correct way to return an error on a pointer
  }
 
  // Register the device driver
  glimmerDevice = device_create(glimmerClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
  if (IS_ERR(glimmerDevice)) {
    class_destroy(glimmerClass);
    unregister_chrdev(majorNumber, DEVICE_NAME);
    pr_alert("GLIMMER: Failed to create the device\n");
    return PTR_ERR(glimmerDevice);
  }

  pr_info("GLIMMER: Registered character device with major number %d\n", majorNumber);
  return result;
}

void glimmer_chardev_exit(void) {
  device_destroy(glimmerClass, MKDEV(majorNumber, 0));  // remove the character device
  class_unregister(glimmerClass);                       // unregister the device class
  class_destroy(glimmerClass);                          // remove the device class
  unregister_chrdev(majorNumber, DEVICE_NAME);          // unregister the major number

  mutex_destroy(&glimmer_mutex);

  pr_info("GLIMMER: Unregistered character device with major number %d\n", majorNumber);
}

/** @brief The device open function that is called each time the device is opened
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_open(struct inode *inodep, struct file *filep) {
  if (!mutex_trylock(&glimmer_mutex)) {  // returns 1 if successful and 0 if there is contention
    pr_alert("GLIMMER: Device in use by another process\n");
    return -EBUSY;
  } else {
    pr_info("GLIMMER: Device opened\n");
    return 0;
  }
}
 
/** @brief This function is called whenever device is being read from user space i.e. data is
 *  being sent from the device to the user.
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 *  @param buffer The pointer to the buffer to which this function writes the data
 *  @param len The length of the b
 *  @param offset The offset if required
 */
static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
   int error_count = 0;
  pr_info("GLIMMER: Device read\n");
   // copy_to_user has the format (*to, *from, size) and returns 0 on success
   error_count = copy_to_user(buffer, message, size_of_message);
 
   if (error_count == 0) {
      printk(KERN_INFO "GLIMMER: Sent %d characters to the user\n", size_of_message);
      return (size_of_message=0);  // clear the position to the start and return 0
   } else {
      printk(KERN_INFO "GLIMMER: Failed to send %d characters to the user\n", error_count);
      return -EFAULT;
   }
}

#define MAX_LENGTH (POV_DISPLAY_WIDTH * POV_DISPLAY_HEIGHT * 3)
static char char_device_buf[MAX_LENGTH];

/** @brief This function is called whenever the device is being written to from user space i.e.
 *  data is sent to the device from the user.
 *  @param filep A pointer to a file object
 *  @param buffer The buffer to that contains the string to write to the device
 *  @param len The length of the array of data that is being passed in the const char buffer
 *  @param offset The offset if required
 */
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset) {
  int nbytes_not_copied;
//  int nbytes; /* Number of bytes written */
//  int bytes_to_do; /* Number of bytes to write */
//  int maxbytes; /* Maximum number of bytes that can be written */

//  pr_info("GLIMMER: Device write\n");
/* 
  maxbytes = MAX_LENGTH - *offset;
 
  if (maxbytes > len) bytes_to_do = len;
  else bytes_to_do = maxbytes;
 
  if (bytes_to_do == 0) {
    pr_info("Reached end of device\n");
    return -ENOSPC; // Returns EOF at write()
  }
 
  nbytes = bytes_to_do -
           copy_from_user(char_device_buf + *offset, // to
                          buffer, // from
                          bytes_to_do); // num bytes
  *offset += nbytes;
*/
  nbytes_not_copied = copy_from_user(char_device_buf, buffer, len);
  if (nbytes_not_copied) return -EFAULT;

//  pr_info("GLIMMER: Received %zu characters from the user\n", len);

  glimmer_update_image(char_device_buf);

  return len;
}
 
/** @brief The device release function that is called whenever the device is closed/released by
 *  the userspace program
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_release(struct inode *inodep, struct file *filep) {
  mutex_unlock(&glimmer_mutex);
  pr_info("GLIMMER: Device closed\n");
  return 0;
}
