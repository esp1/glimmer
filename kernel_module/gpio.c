#include "gpio.h"

unsigned int gpioHall = 61;   // hall effect sensor input on gpio1_29 = 32 + 29

int glimmer_gpio_init(void) {
  int result = 0;

  // Is the GPIO a valid GPIO number (e.g., the BBB has 4x32 but not all available)
  if (!gpio_is_valid(gpioHall)) {
    pr_alert("GLIMMER: Invalid Hall effect sensor GPIO\n");
    return -ENODEV;
  }

  gpio_request(gpioHall, "sysfs");       // Set up the hall effect sensor gpio
  gpio_direction_input(gpioHall);        // Set the hall effect sensor gpio to be an input
//  gpio_set_debounce(gpioButton, 200);      // Debounce the button with a delay of 200ms
  gpio_export(gpioHall, false);          // Causes gpio61 to appear in /sys/class/gpio
                    // the bool argument prevents the direction from being changed

  // Perform a quick check to see that the hall effect sensor is working as expected on LKM load
  pr_info("GLIMMER: Initial hall effect sensor state: %d\n", gpio_get_value(gpioHall));
 
  return result;
}

void glimmer_gpio_exit(void) {
  gpio_unexport(gpioHall);
  gpio_free(gpioHall);
}

