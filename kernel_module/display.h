#ifndef __glimmer_display_h__
#define __glimmer_display_h__


#include <linux/types.h>

extern int glimmer_display_init(void);

extern void glimmer_update_image(uint8_t *buf);

extern void glimmer_display_exit(void);


#endif //__glimmer_display_h__

