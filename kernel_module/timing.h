#ifndef __glimmer_timing_h__
#define __glimmer_timing_h__


#include <linux/kobject.h>
#include <linux/time.h>

extern struct timespec ts_last, ts_current, ts_rotation;  // nano precision

extern int glimmer_timing_init(void);

extern void update_rotation_time(void);

/** @brief Display rotations per second, frames per second */
extern ssize_t rotationTime_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf);

extern uint8_t wait_until_next_display_slice_idx(void);


#endif //__glimmer_timing_h__

