#include <linux/interrupt.h>
#include "gpio.h"
#include "kobject.h"
#include "timing.h"

static unsigned int irqNumber;

// IRQ handler function prototype
static irq_handler_t glimmer_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);

int glimmer_irq_init(void) {
  int result = 0;

  // Initialize timings
  result = glimmer_timing_init();
  if (result) return result;

  // Configure GPIO
  result = glimmer_gpio_init();
  if (result) return result;

  // GPIO numbers and IRQ numbers are not the same! This function performs the mapping for us
  irqNumber = gpio_to_irq(gpioHall);
 
  // Request an interrupt line
  result = request_irq(irqNumber,                            // The interrupt number requested
                       (irq_handler_t) glimmer_irq_handler,  // The pointer to the handler function below
                       IRQF_TRIGGER_RISING,                  // Interrupt on rising edge
                       "glimmer_gpio_handler",               // Used in /proc/interrupts to identify the owner
                       NULL);                                // The *dev_id for shared interrupt lines, NULL is okay
  if (result) {
    pr_alert("GLIMMER: Request for hall effect sensor interrupt line failed with result: %d\n", result);
  } else {
    pr_info("GLIMMER: Hall effect sensor mapped to IRQ %d\n", irqNumber);
  }

  return result;
}

void glimmer_irq_exit(void) {
  free_irq(irqNumber, NULL);  // Free the IRQ number, no *dev_id required in this case

  glimmer_gpio_exit();

  pr_info("GLIMMER: Freed hall effect sensor IRQ %d\n", irqNumber);
}

/** @brief The GPIO IRQ Handler function
 *  This function is a custom interrupt handler that is attached to the GPIO above. The same interrupt
 *  handler cannot be invoked concurrently as the interrupt line is masked out until the function is complete.
 *  This function is static as it should not be invoked directly from outside of this file.
 *  @param irq    the IRQ number that is associated with the GPIO -- useful for logging.
 *  @param dev_id the *dev_id that is provided -- can be used to identify which device caused the interrupt
 *  Not used in this example as NULL is passed.
 *  @param regs   h/w specific register values -- only really ever used for debugging.
 *  return returns IRQ_HANDLED if successful -- should return IRQ_NONE otherwise.
 */
static irq_handler_t glimmer_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs){
  update_rotation_time();

  if (isVerbose) {
    pr_info("GLIMMER: Interrupt! Rotation time: %lu.%.9lu\n", ts_rotation.tv_sec, ts_rotation.tv_nsec);
  }

  return (irq_handler_t) IRQ_HANDLED;      // Announce that the IRQ has been handled correctly
}

