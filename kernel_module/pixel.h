#ifndef __glimmer_pixel_h__
#define __glimmer_pixel_h__


#include <linux/types.h>
#include "pov_geom.h"

// Use uint32_t for idx to avoid byte alignment gaps. See http://www.catb.org/esr/structure-packing/
typedef struct {
  uint64_t red;
  uint64_t green;
  uint64_t blue;
} PixelColor;

typedef struct {
  PixelColor color;
  uint8_t idx;
} PixelData;

typedef struct {
  PixelColor pixels[X_SECTION_WIDTH][LED_STRIP_LENGTH];
} ImageBuffer;


#endif //__glimmer_pixel_h__
