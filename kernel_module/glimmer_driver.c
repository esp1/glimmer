/**
 * @file glimmer_driver.c
 * @author Edwin Park
 * @date 26 October 2017
 * @version 0.1
 * @brief Glimmer POV driver for the BeagleBone Black.
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

#include "kobject.h"
#include "chardev.h"
#include "rpmsg.h"
#include "irq.h"
#include "display.h"

// Module info
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Edwin Park");
MODULE_DESCRIPTION("Glimmer POV driver for the BeagleBone Black.");
MODULE_VERSION("0.1");

/** @brief The LKM initialization function
 *  The static keyword restricts the visibility of the function to within this C file. The __init
 *  macro means that for a built-in driver (not a LKM) the function is only used at initialization
 *  time and that it can be discarded and its memory freed up after that point.
 *  @return returns 0 if successful
 */
static int __init glimmer_init(void) {
  int result = 0;
  pr_info("GLIMMER: Initializing Glimmer POV kernel module\n");

  result = glimmer_display_init();
  if (result) return result;

  result = glimmer_irq_init();
  if (result) return result;

  result = glimmer_kobject_init();
  if (result) return result;

  result = glimmer_chardev_init();
  if (result) return result;

  result = glimmer_rpmsg_init();
  return result;
}

/** @brief The LKM cleanup function
 *  Similar to the initialization function, it is static. The __exit macro notifies that if this
 *  code is used for a built-in driver (not a LKM) that this function is not required.
 */
static void __exit glimmer_exit(void) {
  glimmer_rpmsg_exit();
  glimmer_chardev_exit();
  glimmer_kobject_exit();
  glimmer_irq_exit();
  glimmer_display_exit();

  pr_info("GLIMMER: Exiting Glimmer POV kernel module\n");
}

/** @brief A module must use the module_init() module_exit() macros from linux/init.h, which
 *  identify the initialization function at insertion time and the cleanup function (as listed above)
 */
module_init(glimmer_init);
module_exit(glimmer_exit);

