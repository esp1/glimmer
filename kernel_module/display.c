#include "display.h"
#include "pixel.h"
#include "pov_geom.h"
#include "timing.h"
#include "rpmsg.h"
#include <linux/kthread.h>

static ImageBuffer image_A;
static struct task_struct *task;

void glimmer_update_image(uint8_t *buf) {
  memcpy(&image_A, buf, X_SECTION_WIDTH * LED_STRIP_LENGTH * 24);
}

static uint64_t rotate_color(uint64_t value, uint8_t section) {
  switch (section) {
    case 1: return ((value << 2) & 0xFCFCFCFCFCFCFCFC) + ((value >> 6) & 0x0303030303030303);  // 11 22 33 00
    case 2: return ((value << 4) & 0xF0F0F0F0F0F0F0F0) + ((value >> 4) & 0x0F0F0F0F0F0F0F0F);  // 22 33 00 11
    case 3: return ((value << 6) & 0x3030303030303030) + ((value >> 2) & 0xCFCFCFCFCFCFCFCF);  // 33 00 11 22
  }
  return 0;
}

static PixelData get_pixel_data(uint8_t section, uint8_t section_x, uint8_t section_y) {
  PixelData pixel_data;

  pixel_data.idx = section_y;
  pixel_data.color = image_A.pixels[section_x][section_y];

  if (section > 0) {
    pixel_data.color.red = rotate_color(pixel_data.color.red, section);
    pixel_data.color.green = rotate_color(pixel_data.color.green, section);
    pixel_data.color.blue = rotate_color(pixel_data.color.blue, section);
  }

  return pixel_data;
}

/** @brief The POV display kthread loop
 *
 *  @param arg A void pointer used in order to pass data to the thread
 *  @return returns 0 if successful
 */
static int pov_display(void *arg) {
  uint8_t display_x, section, section_x, section_y;
  PixelData pixel_data;

  pr_info("GLIMMER: Started display thread\n");
  while (!kthread_should_stop()) {  // Returns true when kthread_stop() is called
    set_current_state(TASK_RUNNING);
    // iterate over all pixels in this slice
    for (section_y = 0; section_y < LED_STRIP_LENGTH; section_y++) {
      // read pixel data from image buffer and send to PRU
      pixel_data = get_pixel_data(section, section_x, section_y);
      glimmer_rpmsg_send_pixel_data(pixel_data);
    }

    set_current_state(TASK_INTERRUPTIBLE);
    display_x = POV_DISPLAY_WIDTH - 1 - wait_until_next_display_slice_idx();
    section = display_x / X_SECTION_WIDTH;
    section_x = display_x % X_SECTION_WIDTH;
  }
  pr_info("GLIMMER: Display thread ended\n");
  return 0;
}

int glimmer_display_init(void) {
  // initialize image buffer
  int x, y;
  PixelColor color;
  for (x = 0; x < X_SECTION_WIDTH; x++) {
    for (y = 0; y < LED_STRIP_LENGTH; y++) {
      if (((x % 10) == 0) && ((y % 10) == 0)) {
        // 7:Green 5:Blue   3:Purple 1:White
        // 6:Red   4:Yellow 2:Aqua   0:Gray
        color = (PixelColor) { 0x0000005A00000001,
                               0x0000009600000001,
                               0x0000002E00000001 };
      } else {
        color = (PixelColor) { 0x0000000000000000,
                               0x0000000000000000,
                               0x0000000000000000 };
      }
      image_A.pixels[x][y] = color;
    }
  }

  // start kthread
  task = kthread_run(pov_display, NULL, "glimmer_display_thread");

  return 0;
}

void glimmer_display_exit(void) {
  // stop kthread
  kthread_stop(task);
}

