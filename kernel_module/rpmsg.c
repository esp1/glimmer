#include "rpmsg.h"
#include <linux/rpmsg.h>

static struct rpmsg_channel *rpmsg_chan;

static void glimmer_rpmsg_cb(struct rpmsg_channel *chan, void *data, int len, void *priv, u32 src) {
  static int rx_count;

  pr_info("incoming msg %d (src: 0x%x)\n", ++rx_count, src);
  dev_info(&chan->dev, "incoming msg %d (src: 0x%x)\n", ++rx_count, src);

  print_hex_dump(KERN_DEBUG, __func__, DUMP_PREFIX_NONE, 16, 1, data, len,  true);
}

static int glimmer_rpmsg_probe(struct rpmsg_channel *chan) {
  pr_info("new channel: 0x%x -> 0x%x!\n", chan->src, chan->dst);
  dev_info(&chan->dev, "new channel: 0x%x -> 0x%x!\n", chan->src, chan->dst);
  rpmsg_chan = chan;

  return 0;
}

static void glimmer_rpmsg_remove(struct rpmsg_channel *chan) {
  dev_info(&chan->dev, "glimmer RPMsg driver removed\n");
}

static struct rpmsg_device_id glimmer_rpmsg_id_table[] = {
  { .name = "glimmer" },
  { },
};

static struct rpmsg_driver glimmer_rpmsg_client = {
  .drv.name  = KBUILD_MODNAME,
  .drv.owner = THIS_MODULE,
  .id_table  = glimmer_rpmsg_id_table,
  .probe     = glimmer_rpmsg_probe,
  .callback  = glimmer_rpmsg_cb,
  .remove    = glimmer_rpmsg_remove,
};

int glimmer_rpmsg_init(void) {
  int ret = register_rpmsg_driver(&glimmer_rpmsg_client);
  if (ret) {
    pr_alert("GLIMMER: Error registering RPMsg driver (%d)\n", ret);
  } else {
    pr_info("GLIMMER: Registered RPMsg driver\n");
  }
  return ret;
}

int glimmer_rpmsg_send_pixel_data(PixelData pixel_data) {
  int ret;
  ret = rpmsg_send(rpmsg_chan, &pixel_data, sizeof(PixelData));
  if (ret) {
    pr_err("rpmsg_send failed: %d\n", ret);
    dev_err(&rpmsg_chan->dev, "rpmsg_send failed: %d\n", ret);
  }
  return ret;
}

void glimmer_rpmsg_exit(void) {
  unregister_rpmsg_driver(&glimmer_rpmsg_client);
  pr_info("GLIMMER: Unregistered RPMsg driver\n");
}
