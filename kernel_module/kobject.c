#include <linux/kobject.h>
#include "timing.h"

bool isVerbose = 0;

/** @brief Displays if verbose output is on or off */
static ssize_t isVerbose_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
   return sprintf(buf, "%d\n", isVerbose);
}

/** @brief Stores and sets the verbose output state */
static ssize_t isVerbose_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count) {
   unsigned int temp;
   sscanf(buf, "%du", &temp);  // use a temp varable for correct int->bool
   isVerbose = temp;
   if (isVerbose) {
      pr_info("GLIMMER: Verbose output ON\n");
   } else {
      pr_info("GLIMMER: Verbose output OFF\n");
   }
   return count;
}

/**  Use these helper macros to define the name and access levels of the kobj_attributes
 *  The kobj_attribute has an attribute attr (name and mode), show and store function pointers
 */
static struct kobj_attribute verbose_attr = __ATTR(isVerbose, 0664, isVerbose_show, isVerbose_store);

/**  The __ATTR_RO macro defines a read-only attribute. There is no need to identify that the
 *  function is called _show, but it must be present. __ATTR_WO can be  used for a write-only
 *  attribute but only in Linux 3.11.x on.
 */
static struct kobj_attribute rotationTime_attr  = __ATTR_RO(rotationTime);

/**  The glimmer_attrs[] is an array of attributes that is used to create the attribute group below.
 *  The attr property of the kobj_attribute is used to extract the attribute struct
 */
static struct attribute *glimmer_attrs[] = {
      &rotationTime_attr.attr,  // The difference in time between the last two presses
      &verbose_attr.attr,       // Is verbose output true or false
      NULL,
};

/**  The attribute group uses the attribute array and a name, which is exposed on sysfs
 */
static struct attribute_group attr_group = {
      .name  = "attributes",
      .attrs = glimmer_attrs,  // The attributes array defined just above
};

static struct kobject *glimmer_kobj;

int glimmer_kobject_init(void) {
  int result = 0;

  // create the kobject sysfs entry at /sys/kernel/glimmer
  glimmer_kobj = kobject_create_and_add("glimmer", kernel_kobj);  // kernel_kobj points to /sys/kernel
  if (!glimmer_kobj) {
    pr_alert("GLIMMER: Failed to create kobject mapping\n");
    return -ENOMEM;
  }

  // add the attributes to /sys/kernel/glimmer/
  result = sysfs_create_group(glimmer_kobj, &attr_group);
  if (result) {
    pr_alert("GLIMMER: Failed to create sysfs group\n");
    kobject_put(glimmer_kobj);  // clean up -- remove the kobject sysfs entry
    return result;
  }

  return result;
}

void glimmer_kobject_exit(void) {
  kobject_put(glimmer_kobj);  // remove the kobject sysfs entry
}

