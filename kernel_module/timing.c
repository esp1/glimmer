#include "timing.h"
#include "pov_geom.h"
#include <linux/delay.h>

// acceptable delay range:
// 1 / 30 frames/sec / 56 slices/frame = 595 us/slice
// 10 us < 1/50 of time per slice
// TODO: actually calculate minimum delay based on time to display stuff to strips
#define MIN_DELAY_NS 10000
#define MIN_DELAY_US (MIN_DELAY_NS / 1000)

// Target rotation time is (4 frames/rotation) / (30 frames/sec) = 0.1333 sec/rotation
// Nanosecond resolution is way overkill for rotation timing. Microsecond resolution is more than fine.
struct timespec ts_last, ts_current, ts_rotation;  // nano precision
// using uint32_t to avoid 64-bit division which is not implemented in the linux kernel
static uint32_t rot_ns;        // rotation time in nanoseconds
static uint32_t ns_per_slice;  // nanoseconds per slice

int glimmer_timing_init(void) {
  getnstimeofday(&ts_current);  // get current time
  ts_last = ts_current;         // set the last time to be the current time
  // set the initial rotation time to be 0
  ts_rotation = timespec_sub(ts_current, ts_last);
  rot_ns = 0;

  return 0;
}

void update_rotation_time(void) {
  getnstimeofday(&ts_current);                      // get current time
  ts_rotation = timespec_sub(ts_current, ts_last);  // set rotation time to difference between last and current time
  ts_last = ts_current;                             // set last time to current time

  rot_ns = timespec_to_ns(&ts_rotation);      // store value of rotation time in nanoseconds
  ns_per_slice = rot_ns / POV_DISPLAY_WIDTH;  // calculate nanoseconds per slice
  if (ns_per_slice < MIN_DELAY_NS) ns_per_slice = MIN_DELAY_NS;
}

/** @brief Display the time difference in the form secs.nanosecs to 9 places */
ssize_t rotationTime_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
  // rotations/sec = 1/(sec/rotation)
  uint32_t rot_per_sec = 1000000000 / rot_ns;
  // frames/sec = rotations/sec * frames/rotation
  uint32_t frames_per_sec = rot_per_sec * NUM_X_SECTIONS;
  return sprintf(buf, "%u rotations/sec; %u frames/sec\n", rot_per_sec, frames_per_sec);
}

uint8_t wait_until_next_display_slice_idx(void) {
  struct timespec ts_now, ts_subrot;
  uint32_t subrot_ns, delay_ns;
  uint16_t delay_us;
  uint8_t slice_idx;

  if (ns_per_slice > 0) {
    getnstimeofday(&ts_now);
    // subrotation = now - last time hall sensor was triggered
    ts_subrot = timespec_sub(ts_now, ts_last);
    subrot_ns = timespec_to_ns(&ts_subrot);

    delay_ns = ns_per_slice - (subrot_ns % ns_per_slice);
    if (delay_ns < MIN_DELAY_NS) delay_ns += ns_per_slice;
    delay_us = delay_ns / 1000;
    usleep_range(delay_us, delay_us + MIN_DELAY_US);

    slice_idx = subrot_ns / ns_per_slice;
    return (slice_idx + 1) % POV_DISPLAY_WIDTH;
  } else {
    msleep(1000); // milliseconds
    return 0;
  }
}

