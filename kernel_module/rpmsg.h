#ifndef __glimmer_rpmsg_h__
#define __glimmer_rpmsg_h__


#include "pixel.h"

extern int glimmer_rpmsg_init(void);

extern int glimmer_rpmsg_send_pixel_data(PixelData pixel_data);

extern void glimmer_rpmsg_exit(void);


#endif //__glimmer_rpmsg_h__

