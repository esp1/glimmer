#ifndef __glimmer_gpio_h__
#define __glimmer_gpio_h__


#include <linux/gpio.h>

extern unsigned int gpioHall;

extern int glimmer_gpio_init(void);

extern void glimmer_gpio_exit(void);


#endif //__glimmer_gpio_h__

