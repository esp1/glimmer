#ifndef __glimmer_pov_geom_h__
#define __glimmer_pov_geom_h__


#define NUM_X_SECTIONS 4
#define X_SECTION_WIDTH 56

#define NUM_Y_SECTIONS 2
#define LED_STRIP_LENGTH 51

#define NUM_LED_STRIPS (NUM_X_SECTIONS * NUM_Y_SECTIONS)

#define POV_DISPLAY_WIDTH (NUM_X_SECTIONS * X_SECTION_WIDTH)
#define POV_DISPLAY_HEIGHT (NUM_Y_SECTIONS * LED_STRIP_LENGTH)

#endif //__glimmer_pov_geom_h__

