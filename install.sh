#! /bin/bash

SCRIPT_DIR=$(cd `dirname $0` && pwd)

echo "Building Glimmer kernel module"
cd $SCRIPT_DIR/kernel_module
make

echo "Installing NPM modules"
cd $SCRIPT_DIR/server
npm install

echo "Installing Glimmer systemd services"
cp $SCRIPT_DIR/systemd/* /lib/systemd/system

echo "Enabling Glimmer systemd services"
systemctl enable glimmer
systemctl enable glimmer-http-server
systemctl enable glimmer-websocket-server
