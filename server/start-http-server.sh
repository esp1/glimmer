#! /bin/bash

SCRIPT_DIR=$(cd `dirname $0` && pwd)

PORT=$1
PORT=${PORT:-80}

cd $SCRIPT_DIR
echo "Starting Glimmer HTTP server" > /dev/kmsg
http-server -p $PORT 

