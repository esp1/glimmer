// Generate test image
const image = new Uint8ClampedArray(224 * 102 * 4);
for (var i = 0; i < (224 * 102 * 4); i+=4) {
  image[i  ] =   0;  // R
  image[i+1] =   0;  // G
  image[i+2] = 255;  // B
  image[i+3] = 255;  // A
}

// Encode image
const enc = require('./public/js/glimmer_encoder');
const encoded_image = enc.encode_image(image);

// Sned to POV
const pov = require('./src/glimmer_pov');
pov.update_image(encoded_image);

