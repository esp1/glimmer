const WebSocket = require('ws')
const pov = require('./glimmer_pov')

const ws_port = 8080
const wss = new WebSocket.Server({ port: ws_port })

pov.open()

// load default image /////////////////////////////////////////////
const getPixels = require("get-pixels")
const encode = require('./pov_encode')

getPixels("/home/debian/glimmer/server/public/media/image/earth.jpg", function(err, pixels) {
  if (err) {
    console.log("Bad image path")
    return
  }
  console.log("Display default image", pixels.shape.slice())
  const pov_img = encode.encode_image(pixels)
  pov.update_image(pov_img)
})
///////////////////////////////////////////////////////////////////

var singleton_connection = null

function heartbeat() {
  this.isAlive = true
}

wss.on('connection', (ws) => {
  if (singleton_connection) {
    console.log("terminating connection, another client is already connected")
    ws.terminate()
    return
  }

  console.log("connected")
  singleton_connection = ws

  ws.isAlive = true
  ws.on('pong', heartbeat)
  
  ws.binaryType = 'arraybuffer'
  ws.on('message', (data) => {
    pov.update_image(data)
  })

  ws.on('close', () => {
    console.log("singleton connection closed")
    singleton_connection = null
  })

  ws.on('error', (e) => console.log(e))
});

setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate()

    ws.isAlive = false
    ws.ping('', false, true)
  })
}, 3000)

console.log("Glimmer POV websocket server started on port " + ws_port)
