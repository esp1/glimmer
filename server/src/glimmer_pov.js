const fs = require('fs')

var glimmer_fd

module.exports = {
  open: () => {
    fs.open('/dev/glimmer', 'w', (err, fd) => {
      if (err) throw err
      glimmer_fd = fd
    })
  },

  close: () => {
    fs.close(glimmer_fd, (err) => {
      if (err) throw err
    })
  },
  
  update_image: (arrayBuf) => {
    fs.write(glimmer_fd, Buffer.from(arrayBuf), (err) => {
      if (err) throw err
    })
  }
}
