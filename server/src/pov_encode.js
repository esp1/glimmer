const geom = require('./pov_geom.js')

function get_pixel(pixels, x, y) {
  var r = pixels.get(x, y, 0)
  var g = pixels.get(x, y, 1)
  var b = pixels.get(x, y, 2)
  var a = pixels.get(x, y, 3) / 255.0
  return ((r << 16) + (g << 8) + b) * a
}

function encode_pixels(encoded_image, o, orig_image, x0, y0) {
  const x1 = x0 + geom.POV_SECTION_WIDTH
  const x2 = x1 + geom.POV_SECTION_WIDTH
  const x3 = x2 + geom.POV_SECTION_WIDTH

  const y1 = (geom.POV_HEIGHT - 1) - y0

  // iterate over 24 bit color value
  // red (8 bits), green (8 bits), blue (8 bits)
  for (var i = 0; i < 24; i++) {
    // bit index
    const b = (24 - 1) - i;  // high bit first

    encoded_image[o + i] = (
      // arm 0
      (((get_pixel(orig_image, x0, y0) >>> b) & 1) << 7) + // top
      (((get_pixel(orig_image, x0, y1) >>> b) & 1) << 6) + // bottom

      // arm 1
      (((get_pixel(orig_image, x1, y0) >>> b) & 1) << 5) + // top
      (((get_pixel(orig_image, x1, y1) >>> b) & 1) << 4) + // bottom

      // arm 2
      (((get_pixel(orig_image, x2, y0) >>> b) & 1) << 3) + // top
      (((get_pixel(orig_image, x2, y1) >>> b) & 1) << 2) + // bottom

      // arm 3
      (((get_pixel(orig_image, x3, y0) >>> b) & 1) << 1) + // top
        ((get_pixel(orig_image, x3, y1) >>> b) & 1)         // bottom
    ) >>> 0
  }
}

function encode_image(orig_image) {
  var encoded_image = new Uint8Array(geom.POV_WIDTH * geom.POV_HEIGHT * 3)  // 3 bytes per pixel (1 byte for each color: red, green, blue)

  // output offset
  var o = 0

  // iterate over each pixel in the first display section
  for (var x = 0; x < geom.POV_SECTION_WIDTH; x++) {
    for (var y = 0; y < geom.POV_SECTION_HEIGHT; y++) {
      encode_pixels(encoded_image, o, orig_image, x, y)
      o += 24
    }
  }

  return encoded_image;
}

module.exports = {
  get_pixel,
  encode_pixels,
  encode_image,
}
