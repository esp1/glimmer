#! /bin/bash

SCRIPT_DIR=$(cd `dirname $0` && pwd)

cd $SCRIPT_DIR
echo "Starting Glimmer websocket server" > /dev/kmsg
node src/websocket_server.js > /var/log/glimmer-websocket.log

