(set-env!
  :source-paths #{"src"}
  :dependencies '[[pandeiro/boot-http "0.8.3" :scope "test"]

                  [compojure "1.6.0"]
                  [javax.servlet/servlet-api "2.5"]])

(require '[pandeiro.boot-http :refer [serve]])

(task-options!
  serve {:httpkit true
         :handler 'mecha1.glimmer.server/all-routes
         :port    8000})

(deftask run []
  (comp (javac)
        (serve)
        (wait)))
