// Image scaling ---------------------------------------------------------------

function scale_to_fit(img) {
  const img_aspect = img.width / img.height;

  if (img_aspect < POV_ASPECT) img_scale = POV_HEIGHT / img.height;
  else img_scale = POV_WIDTH / img.width;
}

function draw_scaled_image(g, img, xc, yc, s) {
  g.image(img,
          // destination
          xc - (img.width * s / 2),
          yc - (img.height * s / 2),
          img.width * s,
          img.height * s,
          // source
          0,
          0,
          img.width,
          img.height);
}

// Create modes ----------------------------------------------------------------

function create_mode(config) {
  return {
          name: config.name,
          start: function() {
            this.img = config.load();
          },
          draw: function(g, pov_xc, pov_yc, s) {
            if (this.img) {
              // clear background
              g.background(0);

              draw_scaled_image(g, this.img, pov_xc, pov_yc, s);
            }
          },
          end: function() {
            if (config.end) config.end(this.img);
            this.img = null;
          }
        };
}

const msc_mode = create_mode({
  name: 'MakerSpace Charlotte',
  load: function() {
    return loadImage('media/image/makerspace_logo.png', function(img) { scale_to_fit(img) });
  }
});

const earth_mode = create_mode({
  name: 'Earth',
  load: function() {
    return loadImage('media/image/earth.jpg', function(img) { scale_to_fit(img) });
  }
});

const nyan_mode = create_mode({
  name: 'Nyan Cat',
  load: function() {
    return loadGif('media/animated_gif/nyan.gif', function(img) { scale_to_fit(img) });
  }
});

const mario_mode = create_mode({
  name: 'Super Mario',
  load: function() {
    const video = createVideo('media/video/mario-1.mp4', function() { scale_to_fit(video) });
    video.hide();
    video.loop();
    return video;
  },
  end: function(video) {
    video.stop();
  }
});

const camera_mode = create_mode({
  name: 'Camera',
  load: function() {
    const video = createCapture(VIDEO, function() { scale_to_fit(video) });
    video.hide();
    return video;
  },
  end: function(video) {
    video.stop();
  }
});

// Set mode --------------------------------------------------------------------

var mode;

function set_mode(new_mode) {
  if (mode) mode.end();

  mode = new_mode;
  mode.start();
}

// Draw ------------------------------------------------------------------------

function pov_draw(g) {
  mode.draw(g, POV_WIDTH/2, POV_HEIGHT/2, img_scale);
}

