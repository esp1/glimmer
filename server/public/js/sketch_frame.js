const FPS = 10;

var p5_ready = false;

var sketch_canvas;
var tile_g;
var pov_g;

var img_scale = 1.0;
var img_offset_x = 0;
var img_offset_y = 0;
var img_brightness = 0.0;
var img_contrast = 1.0;

var vx = 0.0;

// Scale -----------------------------------------------------------------------

function sketch_width() {
  return window.innerWidth;
}

// Ratio of sketch units to POV units
function sketch_scale() {
  return sketch_width() / POV_WIDTH;
}

// window onresize handler
window.onresize = function() {
  sketch_canvas.size(sketch_width(), POV_HEIGHT * sketch_scale());
}

// Setup -----------------------------------------------------------------------

function setup() {
  // frame rate
  console.log(FPS + " frames per second");
  frameRate(FPS);

  // sketch canvas
  sketch_canvas = createCanvas(sketch_width(), POV_HEIGHT * sketch_scale());
  sketch_canvas.parent('sketch');

  // pov graphics
  tile_g = createGraphics(POV_WIDTH, POV_HEIGHT);
  tile_g.pixelDensity(1);
  pov_g = createGraphics(POV_WIDTH, POV_HEIGHT);
  pov_g.pixelDensity(1);

  // set initial mode
  set_mode(msc_mode);

  // setup controls
  controls_setup();

  // connect to glimmer
  glimmer_connect();

  p5_ready = true;
}

// Draw ------------------------------------------------------------------------

function draw() {
  // update physics
  img_offset_x = (img_offset_x + vx) % POV_WIDTH;

  // draw stuff
  pov_draw(tile_g);
  adjust_colors(tile_g);

  // draw pov graphics to canvas
  tile_pov_graphics(tile_g, pov_g);
  draw_pov_graphics(pov_g);

  // send pov graphics to server
  if (ws_ready && p5_ready) {
    pov_g.loadPixels();
    glimmer_send(pov_g.pixels);
  }
}

function tile_pov_graphics(tile_g, pov_g) {
  for (var x = img_offset_x - POV_WIDTH; x < POV_WIDTH; x += POV_WIDTH) {
    for (var y = img_offset_y - POV_HEIGHT; y < POV_HEIGHT; y += POV_HEIGHT) {
      pov_g.image(tile_g, x, y);
    }
  }
}

function draw_pov_graphics(pov_g) {
  push();
  scale(sketch_scale());
  
  image(pov_g, 0, 0);
  
  pop();

  draw_pov_pixel_grid(0);
}

function adjust_color_channel(c) {
  return constrain((c * img_contrast) + img_brightness, 0, 255);
}

function adjust_colors(g) {
  g.loadPixels();
  for (var i = 0; i < POV_WIDTH * POV_HEIGHT * 4; i += 4) {
    g.pixels[i] = adjust_color_channel(g.pixels[i]);
    g.pixels[i+1] = adjust_color_channel(g.pixels[i+1]);
    g.pixels[i+2] = adjust_color_channel(g.pixels[i+2]);
  }
  g.updatePixels();
}

function draw_pov_pixel_grid(stroke_color) {
  push();
  stroke(stroke_color);
  strokeWeight(1);

  // draw vertical lines
  for (var x = 0; x < sketch_width(); x += sketch_scale()) {
    line(x,0, x,POV_HEIGHT * sketch_scale());
  }
  // draw horizontal lines
  for (var y = 0; y < (POV_HEIGHT * sketch_scale()); y += sketch_scale()) {
    line(0,y, sketch_width(),y);
  }

  pop();
}

