function get_pixel(pixels, x, y) {
  const i = ((y * POV_WIDTH) + x) * 4;
  return (pixels[i  ] << 16) +  // R
         (pixels[i+1] <<  8) +  // G
          pixels[i+2];          // B
}

function encode_pixels(encoded_image, o, orig_image, x0, y0) {
  const x1 = x0 + POV_SECTION_WIDTH;
  const x2 = x1 + POV_SECTION_WIDTH;
  const x3 = x2 + POV_SECTION_WIDTH;

  const y1 = (POV_HEIGHT - 1) - y0;

  // iterate over 24 bit color value
  // red (8 bits), green (8 bits), blue (8 bits)
  for (var i = 0; i < 24; i++) {
    // bit index
    const b = (24 - 1) - i;  // high bit first

    encoded_image[o + i] = (
      // arm 0
      (((get_pixel(orig_image, x0, y0) >>> b) & 1) << 7) + // top
      (((get_pixel(orig_image, x0, y1) >>> b) & 1) << 6) + // bottom

      // arm 1
      (((get_pixel(orig_image, x1, y0) >>> b) & 1) << 5) + // top
      (((get_pixel(orig_image, x1, y1) >>> b) & 1) << 4) + // bottom

      // arm 2
      (((get_pixel(orig_image, x2, y0) >>> b) & 1) << 3) + // top
      (((get_pixel(orig_image, x2, y1) >>> b) & 1) << 2) + // bottom

      // arm 3
      (((get_pixel(orig_image, x3, y0) >>> b) & 1) << 1) + // top
       ((get_pixel(orig_image, x3, y1) >>> b) & 1)         // bottom
    ) >>> 0;
  }
}

function encode_image(orig_image) {
  var encoded_image = new Uint8Array(POV_WIDTH * POV_HEIGHT * 3);  // 3 bytes per pixel (1 byte for each color: red, green, blue)

  // output offset
  var o = 0;

  // iterate over each pixel in the first display section
  for (var x = 0; x < POV_SECTION_WIDTH; x++) {
    for (var y = 0; y < POV_SECTION_HEIGHT; y++) {
      encode_pixels(encoded_image, o, orig_image, x, y);
      o += 24;
    }
  }

  return encoded_image;
}

// Websocket -------------------------------------------------------------------

var ws_ready = false;

const ws_url = "ws://192.168.1.10:8080";
//const ws_url = "ws://localhost:8080";

var glimmer_ws;

function show_glimmer_connect_error() {
  document.getElementById("glimmer_connect_error").classList.remove("hidden");
}

function hide_glimmer_connect_error() {
  document.getElementById("glimmer_connect_error").classList.add("hidden");
}

function glimmer_connect() {
  console.log("Attempting to connect to Glimmer POV");
  glimmer_ws = new WebSocket(ws_url);
  glimmer_ws.binaryType = "blob";
  glimmer_ws.onopen = function open() {
    hide_glimmer_connect_error();
    console.log("Connected to Glimmer POV websocket at " + ws_url);
    ws_ready = true;
  }
  glimmer_ws.onclose = function close() {
    show_glimmer_connect_error();
    console.log("Websocket connection closed");
    ws_ready = false;
  }
}

function glimmer_send(pixels) {
  const encoded_image = encode_image(pixels);
  glimmer_ws.send(encoded_image);
}

