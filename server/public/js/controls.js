function controls_setup() {
  // hammer.js multitouch setup
  // document.body registers gestures anywhere on the page
  // set options to prevent default behaviors for swipe, pinch, etc
  const hammer = new Hammer(document.body, { preventDefault: true });

  hammer.get('swipe').set({
    direction: Hammer.DIRECTION_ALL
  });
  hammer.get('pinch').set({ enable: true });

  hammer.on('swipe', swipe_velocity);
  hammer.on('pinch', pinch_zoom);
  hammer.on('pinched', pinch_zoom);
  hammer.on('tap', show_control_panel);
}

function show_control_panel() {
  document.getElementById('control_panel').classList.remove('hidden');
}

function hide_control_panel() {
  document.getElementById('control_panel').classList.add('hidden');
}

// Position --------------------------------------------------------------------

var x0;
var y0;

function mousePressed() {
  // image offset
  x0 = mouseX - (img_offset_x * sketch_scale());
  y0 = mouseY - (img_offset_y * sketch_scale());
  vx = 0;
}

function mouseDragged() {
  // move image offset
  img_offset_x = ((mouseX - x0) / sketch_scale()) % POV_WIDTH;
}

// Velocity --------------------------------------------------------------------

function swipe_velocity(e) {
  vx += e.velocityX * 10;
}

// Velocity --------------------------------------------------------------------

function pinch_zoom(e) {
  img_scale = e.scale;
}

// Scale, Brightness, Contrast -------------------------------------------------

function keyPressed() {
  switch (key) {
    // scale
    case 'A': increase_scale(); break;
    case 'Z': decrease_scale(); break;
  }

  switch (keyCode) {
    // brightness
    case UP_ARROW: increase_brightness(); break;
    case DOWN_ARROW: decrease_brightness(); break;

    // contrast
    case RIGHT_ARROW: increase_contrast(); break;
    case LEFT_ARROW: decrease_contrast(); break;
  }

  return false;
}

function increase_scale() {
  img_scale += 0.1;
  console.log('scale: ' + img_scale);
}

function decrease_scale() {
  if (img_scale >= 0.11) img_scale -= 0.1;
  console.log('scale: ' + img_scale);
}

function increase_brightness() {
  img_brightness += 10.0;
  console.log('brightness: ' + img_brightness);
}

function decrease_brightness() {
  img_brightness -= 10.0;
  console.log('brightness: ' + img_brightness);
}

function increase_contrast() {
  img_contrast += 0.5;
  console.log('contrast: ' + img_contrast);
}

function decrease_contrast() {
  img_contrast -= 0.5;
  console.log('contrast: ' + img_contrast);
}

function adjust_contrast(val) {
  img_contrast = val;
}

function adjust_brightness(val) {
  img_brightness = val;
}
