const POV_WIDTH = 224;
const POV_HEIGHT = 102;

const POV_ASPECT = POV_WIDTH / POV_HEIGHT;

const POV_SECTION_WIDTH = 56;
const POV_SECTION_HEIGHT = 51;
